import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './view/home/home.component';
import { LoginComponent } from './view/login/login.component';
import { PremiumComponent } from './view/premium/premium.component';
import { HelpComponent } from './view/help/help.component';
import { NotFoundComponent } from './view/not-found/not-found.component';
import { ArtistaComponent } from './view/artista/artista.component';


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'premium', component: PremiumComponent },
  { path: 'help', component: HelpComponent },
  { path: 'artista', component: ArtistaComponent },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
