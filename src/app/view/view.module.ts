import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { HelpComponent } from './help/help.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { PremiumComponent } from './premium/premium.component';
import { SharedModule } from '../shared/shared.module';
import { ArtistaComponent } from './artista/artista.component';

@NgModule({
  declarations: [
    LoginComponent,
    HomeComponent,
    PremiumComponent,
    HelpComponent,
    NotFoundComponent,
    ArtistaComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    LoginComponent,
    HomeComponent,
    PremiumComponent,
    HelpComponent,
    NotFoundComponent,
    ArtistaComponent
  ]
})
export class ViewModule { }
